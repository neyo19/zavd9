const AddTodo = ({onAdd}) => {
    return (<>
        <form onSubmit={(e)=>{
            e.preventDefault();
            if (e.target.children[0].value)
            onAdd(e.target.children[0].value);
            e.target.children[0].value = ''
        }}>
            <input  id="add-todo" />
        </form>
    </>);
}

export default AddTodo;